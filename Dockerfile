FROM golang:1.16

WORKDIR /go/src/gitlab.com/maxmilia/drone-build
COPY . .

ENV GO111MODULE on
ENV GOOS linux
ENV GOARCH amd64
ENV CGO_ENABLED 0
ENV GOPROXY "https://goproxy.cn,direct"


RUN go build -o release/demo main.go

CMD ['./release/demo']